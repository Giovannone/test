USE [Biblioteca]
GO

/****** Object:  Table [dbo].[Anagrafica]    Script Date: 17/11/2022 09:40:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Anagrafica](
	[Nome] [nchar](12) NULL,
	[Cognome] [nchar](20) NULL,
	[CodiceTessera] [int] NOT NULL,
	[DataDiNascita] [date] NULL,
 CONSTRAINT [PK_Anagrafica] PRIMARY KEY CLUSTERED 
(
	[CodiceTessera] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Libri]    Script Date: 17/11/2022 09:40:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Libri](
	[Titolo] [nvarchar](50) NOT NULL,
	[Autore] [nvarchar](50) NOT NULL,
	[Genere] [nvarchar](50) NOT NULL,
	[Scaffale] [nvarchar](50) NOT NULL,
	[CodiceLibro] [int] NOT NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[TabPrestiti]    Script Date: 17/11/2022 09:40:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TabPrestiti](
	[Cognome] [nvarchar](50) NULL,
	[Titolo] [nvarchar](50) NULL,
	[CodiceLibro] [int] NULL,
	[CodiceTessera] [int] NULL,
	[DataPrestito] [date] NULL,
	[DataRestituzione] [date] NULL,
	[CodPrestito] [nvarchar](50) NULL
) ON [PRIMARY]
GO


